# dwmblocks

Modular status bar for dwm written in C.

Copied from Luke Smith and then modified.

# modifying blocks

The statusbar is made from text output from commandline programs.
Blocks are added and removed by editing the blocks.h header file.

# signaling changes

For example, the audio module has the update signal 10 by default.
Thus, running `pkill -RTMIN+10 dwmblocks` will update it.
