static const Block blocks[] = {
    // Icon Command Update Interval Update Signal
    {"", "vpn", 10, 12},
    {"", "cat /tmp/recordingicon 2>/dev/null", 0, 9},
    //{"", "music", 0, 11},
    {"", "pacpackages", 0, 8},
    //{"", "torrent", 20, 7},
    //{"", "news", 0, 6},
    //{"", "moonphase", 18000, 5},
    //{"", "weather", 18000, 5},
    //{"", "mailbox", 180, 12},
    //{"", "cpu", 5, 0},
    //{"", "disk / 💽", 120, 0},
    {"⌨️ ", "cat $HOME/.cache/xmodmap/current 2>/dev/null",
        0, 14},
    {"", "brightness", 5, 1},
    {"", "volume", 0, 10},
    {"", "battery | tr \'\n\' \' \'", 5, 0},
    {"", "clock", 30, 0},
    {"", "internet", 5, 4},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = '|';
